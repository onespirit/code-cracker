let code = Math.floor(1000 + Math.random() * 9000);
//let code = 9474;


function attempt(){
    let newRow = '';
    let userAttempt = document.getElementsByClassName("player-code")[0].value;

    if(userAttempt == code){
        alert("Number " + userAttempt + " is correct. you won!");
        code = Math.floor(1000 + Math.random() * 9000);
        document.getElementById("history").value = '';
        document.getElementsByClassName("player-code")[0].value = '';
        return;
    }

    let matchedDigitsAmount = 0;
    let matchedExactOrderDigitsAmount = 0;

    let myFunc = num => Number(num);
    let codeArr = Array.from(String(code), myFunc);
    let codeArrCopy = [...codeArr];
    let userAttemptArr = Array.from(String(userAttempt), myFunc);
    let userAttemptArrCopy = [...userAttemptArr];

    matchedDigitsAmount = findDigitsMatches(codeArr, userAttemptArr, matchedDigitsAmount);
    matchedExactOrderDigitsAmount = findExactOrderMatches(codeArrCopy, userAttemptArrCopy, matchedExactOrderDigitsAmount);
    document.getElementsByClassName("player-code")[0].value = '';
    if(document.getElementById("history").value == ''){
        newRow = '';
    } else {
        newRow = '\n';
    }

    document.getElementById("history").value += newRow + userAttempt + " " + matchedDigitsAmount + " " + matchedExactOrderDigitsAmount;
}

function findDigitsMatches(codeArr, userAttemptArr, matchedDigitsAmount){
    for(let i = 0; i < codeArr.length; i++){
        for(let j = 0; j < userAttemptArr.length; j++){
            if(userAttemptArr[j] == codeArr[i]){
                codeArr.splice(i, 1);
                userAttemptArr.splice(j, 1);
                i--;
                matchedDigitsAmount++;
            }
        }
    }

    return matchedDigitsAmount;
}

function findExactOrderMatches(codeArr, userAttemptArr, matchedExactOrderDigitsAmount){
    for(let i = 0; i < codeArr.length; i++){
        if(userAttemptArr[i] == codeArr[i]){
            matchedExactOrderDigitsAmount++;
        }
    }

    return matchedExactOrderDigitsAmount;
}